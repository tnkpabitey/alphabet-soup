def loading_grid(file_path):
    with open(file_path, 'r') as file:
        # From here read the first line to get the number of rows and columns
        dimensions = file.readline().strip().split('x')
        rows = int(dimensions[0])
        cols = int(dimensions[1])

        # From here read the character grid
        grid = []
        for _ in range(rows):
            row = file.readline().strip().split()
            grid.append(row)

        # Read the words to be found
        words = []
        for line in file:
            words.append(line.strip())

    return rows, cols, grid, words


def search_word_grid(rows, cols, grid, word):
    # We define the possible directions: horizontal, vertical, and diagonal
    directions = [(0, 1), (1, 0), (1, 1), (-1, 1)]

    length_of_word = len(word)
    for row in range(rows):
        for col in range(cols):
            # Here we iterate over each position in the grid and check all directions of words
            for direction in directions:
                dx, dy = direction
                end_row = row + (length_of_word - 1) * dx
                end_col = col + (length_of_word - 1) * dy

                # Now We check if the word can fit in the current direction
                if 0 <= end_row < rows and 0 <= end_col < cols:
                    found_forward = True
                    found_backward = True
                    for i in range(length_of_word):
                        char_forward = grid[row + i * dx][col + i * dy]
                        char_backward = grid[end_row - i * dx][end_col - i * dy]

                        if char_forward != word[i]:
                            found_forward = False
                        if char_backward != word[i]:
                            found_backward = False

                    if found_forward:
                        start_pos = f"{row}:{col}"
                        return start_pos, end_pos

                    if found_backward:
                        start_pos = f"{end_row}:{end_col}"
                        end_pos = f"{row}:{col}"
                        return start_pos, end_pos

    return None, None


def search_word_in_grid(rows, cols, grid, words):
    results = []

    for word in words:
        start_pos, end_pos = search_word_grid(rows, cols, grid, word)
        if start_pos is not None:
            results.append((word, start_pos, end_pos))

    return results


def main():
    file_path = input("Enter the path to your file: ")
    rows, cols, grid, words = loading_grid(file_path)
    results = search_word_in_grid(rows, cols, grid, words)

    for result in results:
        word, start_pos, end_pos = result
        print(f"{word} {start_pos} {end_pos}")


if __name__ == "__main__":
    main()